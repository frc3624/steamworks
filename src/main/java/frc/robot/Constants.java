/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

public final class Constants {

    public final static int LEFT_MASTER_ID = 11, LEFT_SLAVE_ID = 13, RIGHT_MASTER_ID = 12, RIGHT_SLAVE_ID = 14;
    public final static int CLIMB_MOTOR_ID = 19;

    public final static int PCM_ID = 0, GEARSHIFT_FORWARD_CHANNEL = 6, GEARSHIFT_REVERSE_CHANNEL = 7;

    public final static int JOYSTICK_ID = 0;

}
